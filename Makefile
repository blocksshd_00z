#
# Makefile for blocksshd
#
# Copyright 2006-2007, James Turnbull & Anton Valqk.
#
#

APPNAME = blocksshd
CONF = blocksshd.conf
INSTALL = install
PREFIX = /usr
EXEC_PREFIX = $(PREFIX)
BINDIR = $(EXEC_PREFIX)/bin
SYSCONFDIR = /etc
MANDIR = $(PREFIX)/man
MAN1DIR = $(MANDIR)/man1
INIT = init/blocksshd
INITDIR = /etc/rc.d/init.d
DESTDIR =

all:	$(APPNAME).1.gz

install: all
	$(INSTALL) -p -m 755 -d $(DESTDIR)$(BINDIR)
	$(INSTALL) -p -m 755 -d $(DESTDIR)$(SYSCONFDIR)
	$(INSTALL) -p -m 755 -d $(DESTDIR)$(MAN1DIR)
	$(INSTALL) -p -m 755 $(APPNAME) $(DESTDIR)$(BINDIR)
	$(INSTALL) -p -m 644 $(CONF) $(DESTDIR)$(SYSCONFDIR)
	$(INSTALL) -p -m 644 $(APPNAME).1.gz $(DESTDIR)$(MAN1DIR)

init:
	$(INSTALL) -m 755 -d $(DESTDIR)$(INITDIR)
	$(INSTALL) -m 755 $(INIT) $(DESTDIR)$(INITDIR)/$(APPNAME)
	
uninstall:
	rm $(DESTDIR)$(BINDIR)/$(APPNAME)
	rm $(DESTDIR)$(SYSCONFDIR)/$(CONF)
	rm $(DESTDIR)$(MANDIR)/$(APPNAME).1.gz
	rm $(DESTDIR)$(INITDIR)/$(APPNAME)

clean:
	rm -fr $(APPNAME).1.gz

$(APPNAME).1.gz: $(APPNAME).man CREDITS
	touch -r $(APPNAME).man timestamp
	cat $(APPNAME).man CREDITS | gzip -c > $(APPNAME).1.gz
	touch -r timestamp $(APPNAME).1.gz

.PHONY: all install init uninstall clean
