.TH BLOCKSSHD 1
.\" NAME should be all caps, SECTION should be 1-8, maybe w/ subsection
.\" other parms are allowed: see man(7), man(1)
.rn SH Sh
.de SH
.br
.ne 11
.Sh "\\$1"
..
.rn SS Ss
.de SS
.br
.ne 10
.Ss "\\$1"
..
.rn TP Tp
.de TP
.br 
.ne 9
.Tp \\$1
..
.rn RS Rs
.de RS
.na
.nf
.Rs
..
.rn RE Re
.de RE
.Re
.fi
.ad
..
.de Sx
.PP
.ne \\$1
.RS
..
.de Ex
.RE
.PP
..
.SH NAME
blocksshd \- Blocks brute force SSH attacks using iptables.
.SH SYNOPSIS
.B blocksshd
.I [\-d \-\-daemon \-\-start] [\-\-stop] [\-h \-\-help] [\-v \-\-version]
.SH "DESCRIPTION"
This manual page documents the
.BR Blocksshd
script.
.PP
.B Blocksshd
is a Perl script based on BruteForceBlocker v1.2.3 that dynamically adds IPTables rules for Linux and pf firewall rules for BSD that block SSH brute force attacks. It can also detect ProFTPd login failures.
BlockSSHD checks a log file you specify, for example /var/log/secure on a Red 
Hat, for SSH login failure messages.  If it detects a failure message it 
records the source IP address and starts a counter.  If messages continue to be
detected from the same source IP address the counter is incremented for each 
message.  When the counter reaches a user-specified threshold then the script
will add a firewall rule blocking SSH connections from that source IP address.
A user-specified time-out is also defined to trigger a reset of the counter. If
the counter is incremented but has not yet reached the blocking threshold and a
new login failure message arrives then BlockSSHD checks the time-out.  If the 
last increment of the counter occurred earlier than the current time minus the 
time-out period then the counter is reset rather than incremented.  The time-out
defaults to 600 seconds (10 minutes).

The BlockSSHD script can also unblock IP address after a period.  This is 
enabled in the blocksshd.conf configuration file using the unblock option and 
with the period set using the unblock_timeout option.

The BlockSSHD script can also log the IP addresses blocked to a file and 
re-apply these blocked IP addresses when the script is re-started.  This allows
you to restore previously blocked IP addresses after a restart or when your 
firewall rules are flushed.  If you have the unblock function and the re-block
function enabled then when the IP address is unblocked it will also be removed
from the log file.

.SH PRE-REQUISITES

BlockSSHD requires the following CPAN modules:

.br
*) Sys::Syslog - often comes with Perl and may already be installed;
.br
*) Sys::Hostname - often comes with Perl and may already be installed;
.br
*) File::Tail;
.br
*) Net::DNS;
.br
*) Proc::Daemon;
.br
*) Proc::PID::File;
.br
*) Getopt::Long;

Please install them prior to running BlockSSHD.

.SH OPTIONS
.TP
.B \-d, \-\-daemon, \-\-start 
.IP "Start BlockSSHD in daemon mode"
.TP
.B \-\-stop 
.IP "Stop BlockSSHD"
.TP
.B \-h, \-\-help 
.IP "Print help text"
.TP
.B \-v, \-\-version
.IP "Print the version number"
.SH EXAMPLES
Start BlockSSHD
.Sx 3
blocksshd --start
.Ex
Stop BlockSSHD
.Sx 3
blocksshd --stop
.Ex
.SH Configuration File
There a number of configuration directives located in this file and these are listed below.  

.br 
*) os - Specify the operating system BlockSSHd will run under.  Use linux for Linux and bsd for BSD
.br
*) pid_file - Location of the BlockSSHd PID file
.br
*) send_email - Enable the sending of email notifications 
.br
*) email - Email address to send email notifications to
.br
*) chain - Name of the iptables chain to hold the rules
.br
*) logfile - Log file to monitor for SSH login failure messages
.br
*) logcheck - Interval to check log file in seconds
.br
*) max_attempts - Maximum number of failures before blocking IP
.br
*) timeout - Time without activity after which IP counts are reset in seconds
.br
*) unblock - Enable unblocking functionality
.br
*) unblock_timeout - Period in seconds since blocking that an IP address is unblocked 
.br
*) restore_blocked - Unable this option to log IP addresses and then re-block them when BlockSSHd is restarted
.br
*) log_ips - Location of the blocked IP address log file
.br
*) mail - Location of the mail binary used to send emails
.br
*) email_whois_lookup - Enable WHOIS lookup of the blocked IP address to be included in blocking notification
.br
*) whois - Location of the whois binary
.br
*) sed - Location of the sed binary
.br
*) iptables - Location of the iptables binary
.br
*) pfctl - Location of the pfctl binary
.br
*) whitelist - A list of IP addresses that you never want blocked
.SH AUTHORS
This manual page was written by James Turnbull <james@lovedthanlost.net>

