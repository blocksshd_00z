Summary:	Blocks brute force SSH attacks using iptables
Name:		blocksshd
Version:	1.3
Release:	1
License:	GPLv2
Group:		Applications/Internet
URL:		http://sourceforge.net/projects/blocksshd/
BuildArch:	noarch
Source:		http://downloads.sourgeforge.net/blocksshd/blocksshd-%{version}.tar.bz2
Requires:	perl >= 5, chkconfig
AutoReq:	no
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root

%description
BlockSSHD is defense against SSHd brute force attacks.
The Perl script is based on BruteForceBlocker v1.2.3.
It dynamically adds IPTables rules in response to SSHd 
brute force attacks..

%prep

%setup -q

%build
%{__make}

%install
%{__rm} -rf "%{buildroot}"

%{__make} install DESTDIR="%{buildroot}" \
	BINDIR="%{_bindir}" \
	SYSCONFDIR="%{_sysconfdir}" \
	MANDIR="%{_mandir}" \
	INITRDDIR="%{_initrddir}" \
	MANCOMPRESS= \
	INSTALL="%{__install} -p"

%{__make} init DESTDIR="%{buildroot}" \
	INITRDDIR="%{_initrddir}"
 
%clean
%{__rm} -rf "%{buildroot}"

%files
%defattr(-,root,root,-)
%doc CHANGELOG README CREDITS INSTALL
%{_bindir}/blocksshd
%{_sysconfdir}/blocksshd.conf
%{_mandir}/man1/blocksshd.1*
%{_initrddir}/blocksshd
%config(noreplace) %{_sysconfdir}/blocksshd.conf

%post
/sbin/chkconfig --add blocksshd

%preun
/sbin/chkconfig --del blocksshd

%changelog
* Fri Jun 27 2008 James Turnbull <james@lovedthanlost.net> 1.3-1
- Removed IPv6 support

* Wed Aug 09 2007 James Turnbull <james@lovedthanlost.net> 1.2-1
- Added new init script
- Changed default blocking list location to /etc
- Documentation fixes

* Tue May 22 2007 James Turnbull <james@lovedthanlost.net> 1.1-2
- Fixed preun field
- Incremented spec file to 1.1-2

* Thu Apr 12 2007 James Turnbull <james@lovedthanlost.net> 1.1-1
- Added -p to spec file
- Changed download source from dl to download
- Changed description
- Added preun option
- Incremented blocksshd.spec to 1.1-1

* Tue Sep 26 2006 James Turnbull <james@lovedthanlost.net> 0.9
- Added sysconfdir installation

* Sun Jul 30 2006 James Turnbull <james@lovedthanlost.net> 0.7
- Added init file support

* Tue Jul 25 2006 James Turnbull <james@lovedthanlost.net> 0.7
- Spec file for Fedora Extras

